package com.kc.statistics.bean;

/**
 * 流量表
 */
public class Statistics {

	private String ar =null;
	private String at =null;
	private String ip =null;
	private String vi =null;
	private String su =null;
	private String si =null;
	private String sb =null;
	private String rnd =null;
	private String ln =null;
	private String ja =null;
	private String fl =null;
	private String ds =null;
	private String cl =null;
	private String ck =null;
	private String ep =null;
	private String stag =null;
	private String tt =null;
	private String nu =null;
	private String lt =null;
	private String bid =null;
	private String sid =null;
	private String ua =null;
	private String netopt =null;
	private String browser =null;
	private String os =null;
	private String devicetype =null;
	
	public String getDevicetype() {
		return devicetype;
	}

	public void setDevicetype(String devicetype) {
		this.devicetype = devicetype;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getNetopt() {
		return netopt;
	}

	public void setNetopt(String netopt) {
		this.netopt = netopt;
	}

	public String getUa() {
		return this.ua;
	}

	public void setUa(String ua) {
		this.ua = ua;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getBid() {
		return this.bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getAr() {
		return this.ar;
	}

	public void setAr(String ar) {
		this.ar = ar;
	}

	public String getAt() {
		return this.at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getVi() {
		return this.vi;
	}

	public void setVi(String vi) {
		this.vi = vi;
	}

	public String getSu() {
		return this.su;
	}

	public void setSu(String su) {
		this.su = su;
	}

	public String getSi() {
		return this.si;
	}

	public void setSi(String si) {
		this.si = si;
	}

	public String getSb() {
		return this.sb;
	}

	public void setSb(String sb) {
		this.sb = sb;
	}

	public String getRnd() {
		return this.rnd;
	}

	public void setRnd(String rnd) {
		this.rnd = rnd;
	}

	public String getLn() {
		return this.ln;
	}

	public void setLn(String ln) {
		this.ln = ln;
	}

	public String getJa() {
		return this.ja;
	}

	public void setJa(String ja) {
		this.ja = ja;
	}

	public String getFl() {
		return this.fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public String getDs() {
		return this.ds;
	}

	public void setDs(String ds) {
		this.ds = ds;
	}

	public String getCl() {
		return this.cl;
	}

	public void setCl(String cl) {
		this.cl = cl;
	}

	public String getCk() {
		return this.ck;
	}

	public void setCk(String ck) {
		this.ck = ck;
	}

	public String getEp() {
		return this.ep;
	}

	public void setEp(String ep) {
		this.ep = ep;
	}

	public String getStag() {
		return this.stag;
	}

	public void setStag(String stag) {
		this.stag = stag;
	}

	public String getTt() {
		return this.tt;
	}

	public void setTt(String tt) {
		this.tt = tt;
	}

	public String getNu() {
		return this.nu;
	}

	public void setNu(String nu) {
		this.nu = nu;
	}

	public String getLt() {
		return this.lt;
	}

	public void setLt(String lt) {
		this.lt = lt;
	}

	public String toString() {
		return "Statistics [ar=" + this.ar + ", at=" + this.at + ", ip=" + this.ip + ", vi=" + this.vi + ", su="
				+ this.su + ", si=" + this.si + ", sb=" + this.sb + ", rnd=" + this.rnd + ", ln=" + this.ln + ", ja="
				+ this.ja + ", fl=" + this.fl + ", ds=" + this.ds + ", cl=" + this.cl + ", ck=" + this.ck + ", ep="
				+ this.ep + ", stag=" + this.stag + ", tt=" + this.tt + ", nu=" + this.nu + ", lt=" + this.lt + "]";
	}

}
