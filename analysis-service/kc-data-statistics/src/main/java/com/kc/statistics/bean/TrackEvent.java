package com.kc.statistics.bean;

/**
 * 自定议事件表
 */
public class TrackEvent {
	private Long id;
	private String ct;
	private String at;
	private String kl;
	private String vl;
	private String lt;
	private String sid;
	private String tag;

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCt() {
		return this.ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public String getAt() {
		return this.at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getKl() {
		return this.kl;
	}

	public void setKl(String kl) {
		this.kl = kl;
	}

	public String getVl() {
		return this.vl;
	}

	public void setVl(String vl) {
		this.vl = vl;
	}

	public String getLt() {
		return this.lt;
	}

	public void setLt(String lt) {
		this.lt = lt;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String toString() {
		return "TrackEvent [id=" + this.id + ", ct=" + this.ct + ", at=" + this.at + ", kl=" + this.kl + ", vl="
				+ this.vl + ", lt=" + this.lt + ", sid=" + this.sid + ", tag=" + this.tag + "]";
	}
}
