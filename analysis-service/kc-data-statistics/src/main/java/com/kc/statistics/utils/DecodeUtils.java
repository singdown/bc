package com.kc.statistics.utils;

import java.net.URLDecoder;

public class DecodeUtils {

    public static void main(String[] args) {
    }

    /**
     * URL解码
     *
     * @param x
     * @return
     */
    public static String decode(String x) {
        try {
            return URLDecoder.decode(x, "utf-8");
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * URL解码
     *
     * @param x
     * @param cl
     * @return
     */
    public static String decode(String x, String cl) {
        try {
            return URLDecoder.decode(x, cl);
        } catch (Exception e) {

        }
        return null;
    }
}
