package com.kc.statistics.httpmodel;

public class StatisticsParms {

	private String ar = "";
	private String at = "";
	private String ip = "";
	private String vi = "";
	private String su = "";
	private String si = "";
	private String sb = "";
	private String rnd = "";
	private String ln = "";
	private String ja = "";
	private String fl = "";
	private String ds = "";
	private String cl = "";
	private String ck = "";
	private String ep = "";
	private String stag = "";
	private String tt = "";
	private String nu = "";
	private String lt = "";
	private String bid = "";
	private String sid = "";
	private String ua = "";

	public String getUa() {
		return this.ua;
	}

	public void setUa(String ua) {
		this.ua = ua;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getBid() {
		return this.bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getAr() {
		return this.ar;
	}

	public void setAr(String ar) {
		this.ar = ar;
	}

	public String getAt() {
		return this.at;
	}

	public void setAt(String at) {
		this.at = at;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getVi() {
		return this.vi;
	}

	public void setVi(String vi) {
		this.vi = vi;
	}

	public String getSu() {
		return this.su;
	}

	public void setSu(String su) {
		this.su = su;
	}

	public String getSi() {
		return this.si;
	}

	public void setSi(String si) {
		this.si = si;
	}

	public String getSb() {
		return this.sb;
	}

	public void setSb(String sb) {
		this.sb = sb;
	}

	public String getRnd() {
		return this.rnd;
	}

	public void setRnd(String rnd) {
		this.rnd = rnd;
	}

	public String getLn() {
		return this.ln;
	}

	public void setLn(String ln) {
		this.ln = ln;
	}

	public String getJa() {
		return this.ja;
	}

	public void setJa(String ja) {
		this.ja = ja;
	}

	public String getFl() {
		return this.fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public String getDs() {
		return this.ds;
	}

	public void setDs(String ds) {
		this.ds = ds;
	}

	public String getCl() {
		return this.cl;
	}

	public void setCl(String cl) {
		this.cl = cl;
	}

	public String getCk() {
		return this.ck;
	}

	public void setCk(String ck) {
		this.ck = ck;
	}

	public String getEp() {
		return this.ep;
	}

	public void setEp(String ep) {
		this.ep = ep;
	}

	public String getStag() {
		return this.stag;
	}

	public void setStag(String stag) {
		this.stag = stag;
	}

	public String getTt() {
		return this.tt;
	}

	public void setTt(String tt) {
		this.tt = tt;
	}

	public String getNu() {
		return this.nu;
	}

	public void setNu(String nu) {
		this.nu = nu;
	}

	public String getLt() {
		return this.lt;
	}

	public void setLt(String lt) {
		this.lt = lt;
	}

}
