package com.kc.statistics.utils;

import java.awt.image.BufferedImage;

public class ImageUtils {

	public static BufferedImage imageins;

	public static synchronized BufferedImage generateImageIo() {
		if (imageins == null) {
			imageins = new BufferedImage(1, 1, 1);
		}

		return imageins;
	}
}