package com.kc.statistics.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.kc.statistics.bean.Statistics;
import com.kc.statistics.bean.TrackEvent;
import com.kc.statistics.httpmodel.MobileVO;

@Repository
public class StatisticsDao {
	private static final Logger log = LoggerFactory.getLogger(StatisticsDao.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * PV、UV
	 * 
	 * @param st
	 */
	public void saveStatistics(Statistics st) {
		try {
			final String sql = "insert into  tb_statistics_data (ar,at,ip,vi,su,si,sb,rnd,ln,ja,fl,ds,cl,ck,stag,tt,nu,lt,bid,sid,ua,netopt,browser,os,devicetype) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
			this.jdbcTemplate.update(sql,
					new Object[] { st.getAr(), st.getAt(), st.getIp(), st.getVi(), st.getSu(), st.getSi(), st.getSb(),
							st.getRnd(), st.getLn(), st.getJa(), st.getFl(), st.getDs(), st.getCl(), st.getCk(),
							st.getStag(), st.getTt(), st.getNu(), st.getLt(), st.getBid(), st.getSid(), st.getUa(),
							st.getNetopt(), st.getBrowser(), st.getOs(), st.getDevicetype() });
		} catch (DataAccessException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	/**
	 * 自定义事件
	 * 
	 * @param te
	 */
	public void savetrackEvent(TrackEvent te, Statistics st) {
		try {
			String sql = " insert into tb_track_event (ct,at,kl,vl,lt,sid,bid,ds,cl,tt,su,vi,ck,ln,ja,si,stag,ua,nu ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
			this.jdbcTemplate.update(sql,
					new Object[] { te.getCt(), te.getAt(), te.getKl(), te.getVl(), te.getLt(), te.getSid(), st.getBid(),
							st.getDs(), st.getCl(), st.getTt(), st.getSu(), st.getVi(), st.getCk(), st.getLn(),
							st.getJa(), st.getSi(), st.getStag(), st.getUa(), st.getNu() });
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	/**
	 * app的流量保存
	 * 
	 * @param vo
	 */
	public void saveMobileVo(MobileVO vo) {
		try {
			String sql = "  insert into tb_app_data ( vi,imei,userid  ,appversion,devicetoken,screenwidth,operator,deviceid,"
					+ " devicemodel,macaddr,connectiontype,bip,lasttime,appname,systemtype,devicename,upagename, "
					+ "systemversion,screenheight,bstag,channel,bid,mpagename,appinstalist,event) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			this.jdbcTemplate.update(sql,
					new Object[] { vo.getVi(), vo.getImei(), vo.getUserid(), vo.getAppversion(), vo.getDevicetoken(),
							vo.getScreenwidth(), vo.getOperator(), vo.getDeviceid(), vo.getDevicemodel(),
							vo.getMacaddr(), vo.getConnectiontype(), vo.getBip(), vo.getLasttime(), vo.getAppname(),
							vo.getSystemtype(), vo.getDevicename(), vo.getUpagename(), vo.getSystemversion(),
							vo.getScreenheight(), vo.getBstag(), vo.getChannel(), vo.getBid(), vo.getMpagename() ,vo.getAppinstalist(),vo.getEvent()});

		} catch (DataAccessException e) {
			e.printStackTrace();
		}

	}

}