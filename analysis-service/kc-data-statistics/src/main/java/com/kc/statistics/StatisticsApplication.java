package com.kc.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;


/**
 * 程序启动入口
 */
@SpringBootApplication
public class StatisticsApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(StatisticsApplication.class, args);
	}


}
