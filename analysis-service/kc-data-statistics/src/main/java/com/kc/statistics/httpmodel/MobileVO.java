package com.kc.statistics.httpmodel;

/**
 * app�����ղ���
 * @author rose
 *
 */
public class MobileVO {

	private String vi;// "1.0.0",
	private String imei;// "863784020990763",
	private String userid;// "863784020990763",
	private String appversion;// "1.0",stago
	private String devicetoken;// "",
	private String screenwidth;// 1080,
	private String operator;// "",
	private String deviceid;// "863784020990763",
	private String devicemodel;// "nubia",
	private String macaddr;// "98:6c:f5:71:cd:eb",
	private String connectiontype;// "WiFi",
	private String bip;// "-1207365204",
	private String lasttime;// 1453950156276,
	// "event": {
	// "event": "onClick"
	// },
	private String appname;// "StaisApplication",
	private String systemtype;// "android",
	private String devicename;// "nubia",
	private String upagename;// "DemoActivity",
	private String systemversion;// "4.4.2",
	private String screenheight;// 1920,
	private String bstag;// "pv_stag",
	private String channel;// "",
	private String bid;// "f599c67af07b42b8a2aa466274fec449",
	private String mpagename;// "DemoActivity",
	
	// "appinstalist": {
	// "weixin": "6.3.8.56_re6b2553",
	// "qq": "6.3.8.56_re6b2553"
	// }
	private String appinstalist;
	private String event;
	public String getAppinstalist() {
		return appinstalist;
	}

	public void setAppinstalist(String appinstalist) {
		this.appinstalist = appinstalist;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getVi() {
		return vi;
	}

	public void setVi(String vi) {
		this.vi = vi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getAppversion() {
		return appversion;
	}

	public void setAppversion(String appversion) {
		this.appversion = appversion;
	}

	public String getDevicetoken() {
		return devicetoken;
	}

	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}

	public String getScreenwidth() {
		return screenwidth;
	}

	public void setScreenwidth(String screenwidth) {
		this.screenwidth = screenwidth;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getDevicemodel() {
		return devicemodel;
	}

	public void setDevicemodel(String devicemodel) {
		this.devicemodel = devicemodel;
	}

	public String getMacaddr() {
		return macaddr;
	}

	public void setMacaddr(String macaddr) {
		this.macaddr = macaddr;
	}

	public String getConnectiontype() {
		return connectiontype;
	}

	public void setConnectiontype(String connectiontype) {
		this.connectiontype = connectiontype;
	}

	public String getBip() {
		return bip;
	}

	public void setBip(String bip) {
		this.bip = bip;
	}

	public String getLasttime() {
		return lasttime;
	}

	public void setLasttime(String lasttime) {
		this.lasttime = lasttime;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getSystemtype() {
		return systemtype;
	}

	public void setSystemtype(String systemtype) {
		this.systemtype = systemtype;
	}

	public String getDevicename() {
		return devicename;
	}

	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}

	public String getUpagename() {
		return upagename;
	}

	public void setUpagename(String upagename) {
		this.upagename = upagename;
	}

	public String getSystemversion() {
		return systemversion;
	}

	public void setSystemversion(String systemversion) {
		this.systemversion = systemversion;
	}

	public String getScreenheight() {
		return screenheight;
	}

	public void setScreenheight(String screenheight) {
		this.screenheight = screenheight;
	}

	public String getBstag() {
		return bstag;
	}

	public void setBstag(String bstag) {
		this.bstag = bstag;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getMpagename() {
		return mpagename;
	}

	public void setMpagename(String mpagename) {
		this.mpagename = mpagename;
	}

//	@Override
//	public String toString() {
//		return "MobileVO [vi=" + vi + ", imei=" + imei + ", userid=" + userid + ", appversion=" + appversion
//				+ ", devicetoken=" + devicetoken + ", screenwidth=" + screenwidth + ", operator=" + operator
//				+ ", deviceid=" + deviceid + ", devicemodel=" + devicemodel + ", macaddr=" + macaddr
//				+ ", connectiontype=" + connectiontype + ", bip=" + bip + ", lasttime=" + lasttime + ", appname="
//				+ appname + ", systemtype=" + systemtype + ", devicename=" + devicename + ", upagename=" + upagename
//				+ ", systemversion=" + systemversion + ", screenheight=" + screenheight + ", bstag=" + bstag
//				+ ", channel=" + channel + ", bid=" + bid + ", mpagename=" + mpagename + ", appinstalist="
//				+ appinstalist + ", event=" + event + "]";
//	}

 

	
	
	
}
