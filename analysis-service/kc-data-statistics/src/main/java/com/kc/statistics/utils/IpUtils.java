package com.kc.statistics.utils;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class IpUtils
{
  public static String getIpAddr(HttpServletRequest request)
  {
    try
    {
      String ip = request.getHeader("X-Real-IP");
      if ((!StringUtils.isBlank(ip)) && (!"unknown".equalsIgnoreCase(ip))) {
        return ip;
      }
      ip = request.getHeader("X-Forwarded-For");
      if ((!StringUtils.isBlank(ip)) && (!"unknown".equalsIgnoreCase(ip)))
      {
        int index = ip.indexOf(',');
        if (index != -1) {
          return ip.substring(0, index);
        }
        return ip;
      }

      return request.getRemoteAddr();
    } catch (Exception e) {
    }
    return "";
  }
}