

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_app_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_app_data`;
CREATE TABLE `tb_app_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `vi` varchar(255) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `appversion` varchar(255) DEFAULT NULL,
  `devicetoken` varchar(255) DEFAULT NULL,
  `screenwidth` double DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `deviceid` varchar(255) DEFAULT NULL,
  `devicemodel` varchar(255) DEFAULT NULL,
  `macaddr` varchar(255) DEFAULT NULL,
  `connectiontype` varchar(255) DEFAULT NULL,
  `bip` varchar(255) DEFAULT NULL,
  `lasttime` varchar(255) DEFAULT NULL,
  `appname` varchar(255) DEFAULT NULL,
  `systemtype` varchar(255) DEFAULT NULL,
  `devicename` varchar(255) DEFAULT NULL,
  `upagename` varchar(255) DEFAULT NULL,
  `systemversion` varchar(255) DEFAULT NULL,
  `screenheight` double DEFAULT NULL,
  `bstag` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `bid` varchar(255) DEFAULT NULL,
  `mpagename` varchar(255) DEFAULT NULL,
  `appinstalist` longtext,
  `event` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_app_data
-- ----------------------------
