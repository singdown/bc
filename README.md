### kc-analysis 用户行为分析系统
欢迎大家提交代码，提交代码请加群联系群主

 **QQ群 ：656595861**

2019-07-19  增加行为分析控制台前端react框架

用户行为分析，是指在获得网站访问量基本数据的情况下，对有关数据进行统计、分析，从中发现用户访问网站的规律，并将这些规律与网络营销策略等相结合，从而发现目前网络营销活动中可能存在的问题，并为进一步修正或重新制定网络营销策略提供依据。这是狭义的只指网络上的用户行为分析。

用户行为分析应该包含以下数据重点分析：
* 用户的来源地区、来路域名和页面；
* 用户在网站的停留时间、跳出率、回访者、新访问者、回访次数、回访相隔天数；
* 注册用户和非注册用户，分析两者之间的浏览习惯；
* 用户所使用的搜索引擎、关键词、关联关键词和站内关键字；
* 用户选择什么样的入口形式（广告或者网站入口链接）更为有效；
* 用户访问网站流程，用来分析页面结构设计是否合理；
* 用户在页面上的网页热点图分布数据和网页覆盖图数据；
* 用户在不同时段的访问量情况等：
* 用户对于网站的字体颜色的喜好程度。

kc-db 图表数据脚本 待完成

kc-Js 前端JS-SDK  完成

kc-src 后端服务   完成

kc-web 后台图表web界面  未完成

启动类


    /**
     * 程序启动入口
     */
    @SpringBootApplication
    public class StatisticsApplication extends SpringBootServletInitializer {
    	public static void main(String[] args) {
    		SpringApplication.run(StatisticsApplication.class, args);
    	}
    }


收集到的数据表
![输入图片说明](https://gitee.com/uploads/images/2018/0209/235300_1cbd0528_302008.png "屏幕截图.png")