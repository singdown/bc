
DROP TABLE IF EXISTS `tb_statistics_data`;
CREATE TABLE `tb_statistics_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID主键',
  `ar` varchar(255) DEFAULT NULL COMMENT '地域',
  `at` varchar(255) DEFAULT NULL COMMENT '服务器访问时间',
  `ip` varchar(255) DEFAULT NULL COMMENT '访问IP',
  `vi` varchar(255) DEFAULT NULL COMMENT 'JS版本',
  `su` varchar(255) DEFAULT NULL COMMENT '上一页url',
  `si` varchar(255) DEFAULT NULL COMMENT '统计代码id',
  `sb` varchar(255) DEFAULT NULL COMMENT '如果是360se浏览器该值等于‘17’',
  `rnd` varchar(255) DEFAULT NULL COMMENT '十位随机数字',
  `ln` varchar(255) DEFAULT NULL COMMENT '浏览器语言',
  `ja` varchar(255) DEFAULT NULL COMMENT 'java支持',
  `fl` varchar(255) DEFAULT NULL COMMENT 'flash版本',
  `ds` varchar(255) DEFAULT NULL COMMENT '屏幕尺寸,如 ''1024x768''',
  `cl` varchar(255) DEFAULT NULL COMMENT '颜色深度 如 “32-bit”',
  `ck` varchar(255) DEFAULT NULL COMMENT '是否支持cookie 1:0',
  `stag` varchar(255) DEFAULT NULL COMMENT '事件名称、标签名称',
  `tt` varchar(255) DEFAULT NULL COMMENT '网页标题',
  `nu` varchar(255) DEFAULT NULL COMMENT '当前url位置',
  `lt` varchar(255) DEFAULT NULL COMMENT '浏览器最近刷新时间',
  `bid` varchar(255) DEFAULT NULL COMMENT 'cookie 用户id',
  `sid` varchar(255) DEFAULT NULL COMMENT '登录用户id',
  `ua` varchar(255) DEFAULT NULL COMMENT '浏览器ua',
  `netopt` varchar(255) DEFAULT NULL COMMENT '网络运营商',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器版本、Firefox 42',
  `os` varchar(255) DEFAULT NULL COMMENT '操作系统、Windows 10 / android ....',
  `devicetype` varchar(255) DEFAULT NULL COMMENT '设备类型COMPUTER、MOBILE......',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tb_track_event`;
CREATE TABLE `tb_track_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ct` varchar(255) DEFAULT NULL COMMENT '要监控的目标的类型名称',
  `at` varchar(255) DEFAULT NULL COMMENT '用户跟目标交互的行为',
  `kl` varchar(255) DEFAULT NULL COMMENT 'key',
  `vl` varchar(255) DEFAULT NULL COMMENT '值',
  `lt` varchar(255) DEFAULT NULL COMMENT '最后访问时间',
  `sid` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `bid` varchar(255) DEFAULT NULL COMMENT '客户端ID',
  `ds` varchar(255) DEFAULT NULL COMMENT '屏幕分辨率',
  `cl` varchar(255) DEFAULT NULL,
  `tt` varchar(255) DEFAULT NULL COMMENT '网页标题',
  `su` varchar(255) DEFAULT NULL COMMENT '上一页url',
  `vi` varchar(255) DEFAULT NULL COMMENT 'JS版本',
  `ck` varchar(255) DEFAULT NULL COMMENT '是否支持cookie 1:0',
  `ln` varchar(255) DEFAULT NULL COMMENT '浏览器语言',
  `ja` varchar(255) DEFAULT NULL COMMENT 'java',
  `si` varchar(255) DEFAULT NULL COMMENT '统计代码id',
  `stag` varchar(255) DEFAULT NULL,
  `ua` varchar(255) DEFAULT NULL COMMENT '浏览器ua',
  `nu` varchar(255) DEFAULT NULL COMMENT '当前url位置',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `seitid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `reg_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `alias` varchar(255) DEFAULT NULL COMMENT '别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for tb_app_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_app_data`;
CREATE TABLE `tb_app_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `vi` varchar(255) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `appversion` varchar(255) DEFAULT NULL,
  `devicetoken` varchar(255) DEFAULT NULL,
  `screenwidth` double DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `deviceid` varchar(255) DEFAULT NULL,
  `devicemodel` varchar(255) DEFAULT NULL,
  `macaddr` varchar(255) DEFAULT NULL,
  `connectiontype` varchar(255) DEFAULT NULL,
  `bip` varchar(255) DEFAULT NULL,
  `lasttime` varchar(255) DEFAULT NULL,
  `appname` varchar(255) DEFAULT NULL,
  `systemtype` varchar(255) DEFAULT NULL,
  `devicename` varchar(255) DEFAULT NULL,
  `upagename` varchar(255) DEFAULT NULL,
  `systemversion` varchar(255) DEFAULT NULL,
  `screenheight` double DEFAULT NULL,
  `bstag` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `bid` varchar(255) DEFAULT NULL,
  `mpagename` varchar(255) DEFAULT NULL,
  `appinstalist` longtext,
  `event` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_app_data
-- ----------------------------
